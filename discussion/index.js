/*
	while loop

	a while loop takes ina expression or condition

	if the condition evaluates to be true, the statements inside the code will executed

	syntax:
		while(expression/condition){
			statement
		}
*/

let count = 5;
// while the value of count is not  equal to zero, it will run
while(count !== 0){
	// the current value of count is print out
	console.log("While: "+count);
	// decreases the value of count by 1 after every iteration
	count--;
}


/*
	do while

	works a lot like the while loop but it guarantees that the code will be executed atleast once.

	syntax:
		do{
			statement
		}while(expression)
*/

let number = Number(prompt("give me a number"));
do{
	// the current value of number is printed out
	console.log("Do while: "+ number);

	// increase the value of number by 1 after every iteration
	number+=1;

	// providing a number of 10 or greater will run the code block once but the loop will stop there
}while(number <= 10);



/*
	FOR LOOP

	a for loop is more flexible than while and do-while loops.

	it contains of three parts
		- the initial value which is the starting value. The count will start at the initial value
		- the expression or condition that will determine if the loop will run one more time. While his is true, the loop will run
		- the finalExpression which determines how the loop will run (increment or decrement)


	SYNTAX:

		for(initial; expression; finalExpression){
	statement
		}
*/

console.log("for")
for(let count = 0; count <= 20; count++){
	console.log(count);
}

let myString = "string";

/*console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);
console.log(myString[4]);
console.log(myString[5]);*/

// this loop prints the individual letters of the mystring variable
for(let x=0; x<myString.length; x++){
	console.log(myString[x]);
}



let myName = "patrickabad"

for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() === "a" ||
		myName[i].toLowerCase() === "e" ||
		myName[i].toLowerCase() === "i" ||
		myName[i].toLowerCase() === "o" ||
		myName[i].toLowerCase() === "u"
		){
		console.log(3)
	}else{
		console.log(myName[i]);
	}
}


/* 
	CONTINUE AND BREAK

	the CONTINUE statement allows the code to go to the next iteration of a loop without finishing the execution of a statements in a code block.

	the BREAK statement is uded to terminate the loop once a match has been found
*/

	console.log("###########################")



for (let count=0; count<=20; count++){
	// if remainder is equal to 0
	if(count%2 === 0){
		// tells the code to continue to the next iteration of the loop
		// ignores all the statements located after the continue statement
		continue;
	}
	// the current value of number is printed out if the remainde is not equal to 0
	console.log("continue and break: "+count)
	if (count > 10) {
		// tells the code to terminate or stop the loop defines that it should execute so long as the value of count is less than or equal to 20
		break;
	}
}


	console.log("###########################")



	let name="jakelester";

	for(let i = 0; i < name.length; i++){
		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){
			console.log("continue to the next iteration")
		}

		if(name[i] == "d"){
			break;
		}
	}